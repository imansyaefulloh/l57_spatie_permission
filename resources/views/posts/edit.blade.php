@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h1>Create New Post</h1>
            <hr>
            <div class="form-group">
                {{ Form::model($post, array('route' => 'posts.update', $post->id)) }}
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', null, array('class' => 'form-control')) }}
                    <br>
                    {{ Form::label('body', 'Post Body') }}
                    {{ Form::textarea('body', null, array('class' => 'form-control')) }}

                    {{ Form::submit('Save',array('class' => 'btn btn-success btn-block')) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection