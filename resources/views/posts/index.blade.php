@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Posts</h3>
                    </div>
                    <div class="panel-heading">
                        Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}
                    </div>
                    @foreach($posts as $post)
                        <div class="panel-body">
                            <li style="list-style-type: disc">
                                <a href="{{ route('posts.show', $post->id) }}">
                                    <p class="teaser">
                                        {{ str_limit($post->body, 100) }}
                                    </p>
                                </a>
                            </li>
                        </div>
                    @endforeach
                </div>
                <div class="text-center">
                    {!! $posts->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection